//
//  LucyApp.swift
//  Lucy
//
//  Created by zuzu on 10/11/2023.
//

import SwiftUI

@main
struct LucyApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
